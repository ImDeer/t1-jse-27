package t1.dkhrunina.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error: user id is empty");
    }

}